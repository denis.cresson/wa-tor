# SIMULATION WA-TOR

## Qu'est ce donc

Wa-Tor est une simulation de type proie-prédateur. Dans une mer "torique" évoluent des thons (les proies) et des requins (les prédateurs). Les uns et les autres se déplacent et se reproduisent. Pour acquérir l'énergie suffisante à sa survie un requin doit manger un thon régulièrement. Un thon vit éternellement tant qu'il n'est pas mangé par un requin.

## La mer

La mer est représentée par une grille à deux dimensions torique, c'est à dire que chaque case a quatre voisines, une dans chacune des quatre directions cardinales (on ne s'occupera pas des diagonales).  

on remarque par exemple qu'une case du bas de la grille aura pour voisin une case du haut de celle-ci comme si la grille était refermée pour former une sorte de cylindre, même chose pour une case de la rangée de droite. C'est la conjonction de ces phénomènes qui donne l'aspect torique de la mer  

![tore ](images/img390.jpg)

Chaque case de cette grille représente une zone de mer qui peut être soit vide, soit occupée par un thon ou un requin.

 

## Les poissons

Chaque thon est caractérisé par son temps de gestation. Ce temps est initialisé à une valeur initiale commune à tous les thons, appelée durée de gestation des thons.
Chaque requin est caractérisé par son temps de gestation et son énergie. Ces deux valeurs sont initialisées à une valeur initiale commune à tous les requins, appelées respectivement durée de gestation des requins et énergie des requins.  
On définira donc trois constantes:
```python
TEMPS_DE_GESTATION_THON=2
TEMPS_DE_GESTATION_REQUIN=5
ENERGIE_REQUIN=3
```

## Représentation d'une grille

Du point de vue technique une grille du jeu de la vie sera représentée par une
liste de listes de liste. le dernier niveau de liste permettra de définir si:
* la case ne contient personne  []
* la case contient un thon [3]
* la case contient un requin [2,1]

Par exemple la liste :  
```python
[[[],[1],[1,2]],[[1,2],[],[]],[[2,2],[],[3]],[[],[2],[]]]  
```
représente une grille de jeu de 12 cases, 3 cases en largeur et 4 cases en
hauteur. Sur la quatrième ligne seule la deuxième case contient un thon dont le temps de gestation est à 2,
tandis que sur la deuxième ligne, seule la première contient un requin dont le temps de gestation est à 1 et l’énergie à 2.

## Construction d'une grille vide

Réalisez une fonction creer_grille qui prend en paramètre le nombre de cases
horizontalement, puis verticalement et qui renvoie une liste de listes
correspondant à une grille du jeu de la vie aux dimensions souhaitées, ne
contenant que des listes vides.

```python
>>>grille_vide(3, 2)
[[[], [], []], [[], [], []]]
```

## Dimensions d'une grille

Réalisez une fonction hauteur qui prend en paramètre une grille et qui renvoie le nombre de cases verticalement.
```python
>>> hauteur(grille_vide(3, 2))
2
```
Réalisez une fonction largeur qui prend en paramètre une grille et qui renvoie le nombre de cases horizontalement.
```python
>>> largeur(creer_grille(3, 2))
3
```
Réalisez une fonction nombre_de_cases qui prend en paramètre une grille et qui renvoie le nombre de cases de celle-ci.
```python
>>> nombre_de_cases(grille_vide(3, 2))
6
```

## Nombre de positions possibles
Réaliser une fonction pos_possibles qui prend en paramètre une grille et renvoie une liste des positions (i,j) associés au cases de la grille

```python
>>> pos_possibles(grille_vide(3, 2))
[(0,0),(0,1),(1,0),(1,1),(2,0),(2,1)]
```

## Ajouter un poisson
Réaliser deux fonctions ajoute_un_thon et ajoute_un_requin qui prennent une grille en paramètre ainsi qu'une position (i,j) et ajoute le bon poisson au bon endroit.  
On rappelle qu'au départ un requin est défini par [TEMPS_DE_GESTATION_REQUIN,ENERGIE_REQUIN] et un thon [TEMPS_DE_GESTATION_THON].  
```python
>>>g=grille_vide(4, 3)
>>>pos=(1,2)
>>> ajoute_un_thon(g,pos)
>>>g
[[[],[],[],[]],[[],[],[],[]],[[],[2],[],[]]]
>>>pos=(2,0)
>>> ajoute_un_requin(g,pos)
>>>g
[[[],[],[5,3],[]],[[],[],[],[]],[[],[2],[],[]]]

```
## Supprime une case

Réaliser une fonction efface_une_case qui prend en paramètre une grille et une position et qui efface la case à cette position

```
>>> g=grille_vide(4, 3)
>>> pos=(2,0)
>>> ajoute_un_thon(g,pos)
>>> g
[[[], [], [5,3], []], [[], [], [], []], [[], [], [], []]]
>>>efface_une_case(g,pos)
>>>g
[[[], [], [], []], [[], [], [], []], [[], [], [], []]]
```

## Remplir la mer

Réaliser une fonction qui prend en paramètre une grille et y introduit de manière aléatoire un nombre de thons et de requins entré lui aussi en paramètre puis la retourne.  
on pourra par exemple choisir successivement et de manière aléatoire une case dans la liste des positions de la grille (récupérée avec la fonction précédente) et la supprimer avant de passer à la suivante

```python
>>> g=grille_vide(4, 3)
>>> g
[[[], [], [], []], [[], [], [], []], [[], [], [], []]]
>>> remplir_grille(g,2,1)
[[[2], [], [], []], [[], [], [2], []], [[5, 3], [], [], []]]
>>> 
```

## Nature d'une case
Ecrire trois fonctions est_libre est_thon et est_requin qui prennent en paramètre une grille et une position et qui renvoie un booléen True si la condition est vérifiée et False sinon

```python
>>>g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
>>> est_libre(g,(2,1))
False
>>> est_requin(g,(3,1))
False
>>> est_thon(g,(0,0))
True
```

## Determination des voisins
Réaliser une fonction voisins qui prend en paramètre une grille remplie et une position (i,j) et renvoie la liste des positions voisines (les quatre)

```python
>>>g=grille_vide(4, 3)
>>>voisins(g,(2,1))
[(1, 1), (3, 1), (2, 0), (2, 2)]
>>>voisins(g,(3,1))
[(2, 1), (0, 1), (3, 0), (3, 2)]
>>>voisins(g,(1,0))
[(0, 0), (2, 0), (1, 2), (1, 1)]
```

## Détermination des voisins libres
Réaliser une fonction voisins_libres qui prend en paramètre une grille remplie et une position (i,j) et renvoie la liste des positions voisines non occupées

```python
>>>g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
>>>voisins_libres(g,(2,1))
[(1, 1), (2, 0)]
>>>voisins_libres(g,(3,1))
[]
>>>voisins_libres(g,(1,0))
[(2, 0), (1, 2), (1, 1)]
```

## Détermination des voisins occupés par des thons
Réaliser une fonction voisins_thons qui prend en paramètre une grille remplie et une position (i,j) et renvoie la liste des positions voisines occupées par des thons

```python
>>>g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
>>>voisins_thons(g,(2,1))
[(3, 1)]
>>>voisins_thons(g,(3,1))
[(2, 1), (0, 1), (3, 2)]
>>>voisins_thons(g,(1,0))
[(0, 0)]
```
## Déplacement d'un poisson
Réaliser une fonction déplacer prenant en paramètre une grille et deux positions et qui déplace un poisson d'une position à une autre  
Remarque: on effacera le contenu de la case de départ.

```python
>>>g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
>>>deplacer(g,(3,0),(3,1))
>>>g
>>>[[[2], [], [], []], [[2], [], [2], [5,3]], [[5, 3], [], [5, 3], [2]]]
>>> deplacer(g,(3,2),(3,0))
>>> g
[[[2], [], [], [2]], [[2], [], [2], [5, 3]], [[5, 3], [], [5, 3], []]]
```

## Choix aléatoire d'une position dans une liste

Réaliser une fonction qui prend en paramètre une liste de positions (i,j) qui renvoie une des position de manière aléatoire

```python
>>> liste=[(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2), (2, 0), (2, 1), (2, 2), (3, 0), (3, 1), (3, 2)]
>>> position_aleatoire(liste)
(1, 2)

```
## Baisse du taux de gestation

Réaliser une fonction qui prend en paramètre une grille et une position, modifie la grille en diminuant le taux et retourne le taux

```python
>>>g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
>>>baisse_gestation(g,(3,2))
1
>>>g
[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [1]]]
```
## Baisse du taux d’énergie

Réaliser une fonction qui prend en paramètre une grille et une position, modifie la grille en diminuant le taux et retourne le taux

```python
>>>g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
>>>baisse_energie(g,(3,0))
1
>>>g
[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [1]]]
```
## Comportement des thons
Réaliser une fonction comportement_thon prenant en paramètre une grille et une position qui en modifiant la grille gère successivement

*le déplacement en choisissant aléatoirement une position dans la liste des cases libres voisines si c'est possible
*gère la reproduction en 
    * diminuant le taux de gestation
    * si le taux est arrivé à 0
        * si il y a eu déplacement ajoute un thon à la case initiale
        * met le taux de gestation à TEMPS_DE_GESTATION_THON

## Comportement des requins
Réaliser une fonction comportement_requin prenant en paramètre une grille et une position qui en modifiant la grille gère successivement

* le niveau d’énergie du requin est diminué de 1
* le déplacement en choisissant aléatoirement une position 
    * dans la liste des cases contenant un thon voisines si c'est possible, le requin se déplace alors vers cette case et mange le thon (ou le remplace sur la case) son énergie repasse à ENERGIE_REQUIN
    * à défaut dans une case vide voisine
* sa mort si son niveau d'énergie est à 0 (on efface la case) dans le cas contraire on gère la reproduction en 
    * diminuant le taux de gestation
    * si le taux est arrivé à 0
        * si il y a eu déplacement ajoute un thon à la case initiale
        * met le taux de gestation à TEMPS_DE_GESTATION_REQUIN
     
## Calcul de la nouvelle grille après un pas
Réaliser une fonction pas_suivant qui prend en paramètre une grille et la modifie en place de la manière suivante

* choisir une case de manière aléatoire parmi la liste des positions possibles de la grille
* applique suivant la nature de la case le comportement adapté
    * si c'est un thon celui des thons
    * si c'est un requin celui des requins
    * sinon rien bien sur

## Visualisation de la mer
Vous pouvez réaliser une fonction qui visualise la grille en paramètre en utilisant du texte puis appeler régulièrement cette fonction dans une fonction plus générale prenant en paramètre la hauteur de la grille et sa largeur ainsi que le nombre de pas (à multiplier par la taille) de transformation ( prévoir un appel tout les 100 pas par exemple)

sinon vous pouvez utiliser les deux fonctions suivantes (basées sur le module pylab):
![simulation](images/simulation.png)
```python
from pylab import *
def previsualisation(grille,i,l,h,p):
    """
    préaffiche la grille en utilisant des pylab(sans show)
    :param grille:(list)
    :param i:(int) le pas
    :param l:(int) la largeur
    :param h:(int) la hauteur
    :param p:(int) le nombre de pas
    :CU:None
    """
    abst=[]
    ordt=[]
    absr=[]
    ordr=[]
    title('simulation pour largeur:'+str(l)+' hauteur:'+str(h)+' pas:'+str(p) +'\n'+'itération:'+str(i))
    axis([-1,len(grille[0]),-1,len(grille)])
    for h in range(len(grille)):
        for l in range(len(grille[0])):
            if len(grille[h][l])==1:
                abst.append(l)
                ordt.append(len(grille)-h-1)
    plot(abst, ordt,"bo");
    for h in range(len(grille)):
        for l in range(len(grille[0])):
            if len(grille[h][l])==2:
                absr.append(l)
                ordr.append(len(grille)-h-1)
    plot(absr, ordr,"ro");
    draw()

def visualisation_mer(largeur,hauteur,pas):
    """
    Crée une grille de la bonne taille avec le bon pourcentage de poissons
    puis la fait évoluer le bon nombre de fois en appelant la prévisualisation  
    tout les 100 pas et pour finir affiche la visualisation
    :param largeur:(int)
    :param hauteur:(int)
    :param pas:(int)
    """
    taille=largeur*hauteur
    grille=remplir_grille(grille_vide(largeur,hauteur),(taille*POURC_THON)//100,(taille*POURC_REQUIN)//100)
    previsualisation(grille,0,largeur,hauteur,pas)
    clf()
    for i in range(pas*nombre_de_cases(grille)):
        pas_suivant(grille)
        if i%100==0:
            previsualisation(grille,i,largeur,hauteur,pas)
            pause(0.1)
            clf()
    show();
```
## Courbes représentatives

Modifier le programme de manière à avoir trois variables globales 
* th pour le nombre de thons
* re pour celui de requins
* pasencours pour la valeur du pas correspondant à la grille

Ne pas oublier de modifier ces valeurs lors de la naissance ou la mort d'un poisson

Pour visualiser le résultat on pourra par exemple utiliser la fonction suivante basée sur le module pylab
![courbes](images/courbes.png)
```python
from pylab import *
def simulation(largeur,hauteur,pas):
    """
    Crée une grille de la bonne taille avec le bon pourcentage de poissons puis affiche le graphique des effectifs de thon et de requins au cours de la simulation
    :param largeur:(int)
    :param hauteur:(int)
    :param pas:(int)
    """
    taille=largeur*hauteur
    grille=remplir_grille(grille_vide(largeur,hauteur),(taille*POURC_THON)//100,(taille*POURC_REQUIN)//100)
    ths=[th]
    res=[re]
    lesx=[pasencours]
    for i in range(pas*nombre_de_cases(grille)):
        pas_suivant(grille)
        ths.append(th)
        res.append(re)
        lesx.append(pasencours)
    plot(lesx,ths,"b-");
    plot(lesx,res,"r-");
    axis([-1,len(lesx),-1,max(max(ths),max(res))])
    title('simulation pour largeur:'+str(largeur)+' hauteur:'+str(hauteur)+' pas:'+str(pas))

    show();
```
# Code final

```python
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  3 09:46:34 2019

@author: Denis CRESSON
"""
from random import randint
from pylab import *

TEMPS_DE_GESTATION_THON=2
TEMPS_DE_GESTATION_REQUIN=5
ENERGIE_REQUIN=3
POURC_THON=30
POURC_REQUIN=10

th=0
re=0
pasencours=0

def grille_vide(largeur,hauteur):
    """
    Construit une grille vide de la bonne taille (la grille contiendra des listes vides)
    :param largeur:(int)
    :param hauteur:(int)
    :CU: None
    
    >>> grille_vide(3,2)
    [[[], [], []], [[], [], []]]
    """
    
    return [[[] for i in range(largeur)] for j in range(hauteur)]

 
def largeur(grille):
    """
    renvoie la largeur d'une grille
    :param grille:(list)
    :return: (int)
    
    >>> largeur(grille_vide(3,2))
    3
    """
    return len(grille[0])

def hauteur(grille):
    """
    renvoie la hauteur d'une grille
    :param grille:(list)
    :return: (int)
    
    >>>hauteur(grille_vide(3,2))
    2
    """
    return len(grille)
    
def nombre_de_cases(grille):
    """
    renvoie le nombre de cases d'une grille
    :param grille:(list)
    :return: (int)
    
    >>> nombre_de_cases(grille_vide(3,2))
    6
    """
    return largeur(grille)*hauteur(grille)

def pos_possibles(grille):
    """
    renvoie une liste des positions (i,j) associés au cases de la grille
    :param grille:(list)
    :return: (int)
    >>> pos_possibles(grille_vide(3, 2))
    [(0,0),(0,1),(1,0),(1,1),(2,0),(2,1)]
    """
    retour=[]
    for i in range(largeur(grille)):
        for j in range(hauteur(grille)):
            retour.append((i,j))
    return retour

def ajoute_un_thon(grille,pos):
    """
    modifie la grille en place en ajoutant un thon à la position donnée
    :param grille:(list)
    :param pos: (tuple) (i,j) avec i et j entiers
    :Effet de bord: modifie la grille
    
    >>> g=grille_vide(4, 3)
    >>> pos=(1,2)
    >>> ajoute_un_thon(g,pos)
    >>> g
    [[[], [], [], []], [[], [], [], []], [[], [2], [], []]]
    """
    grille[pos[1]][pos[0]]=[TEMPS_DE_GESTATION_THON]

def ajoute_un_requin(grille,pos):
    """
    modifie la grille en place en ajoutant un requin à la position donnée
    :param grille:(list)
    :param pos:(tuple) (i,j) avec i et j entiers
    :Effet de bord: modifie la grille
    
    >>> g=grille_vide(4, 3)
    >>> pos=(2,0)
    >>> ajoute_un_thon(g,pos)
    >>> g
    [[[], [], [5,3], []], [[], [], [], []], [[], [], [], []]]
    """
    grille[pos[1]][pos[0]]=[TEMPS_DE_GESTATION_REQUIN,ENERGIE_REQUIN]

def efface_une_case(grille,pos):
    """
    modifie la grille en place en supprimant la case
    :param grille:(list)
    :param pos:(tuple) (i,j) avec i et j entiers
    :Effet de bord: modifie la grille
    
    >>> g=grille_vide(4, 3)
    >>> pos=(2,0)
    >>> ajoute_un_thon(g,pos)
    >>> g
    [[[], [], [5,3], []], [[], [], [], []], [[], [], [], []]]
    >>>efface_une_case(g,pos)
    >>>g
    [[[], [], [], []], [[], [], [], []], [[], [], [], []]]
    """
    grille[pos[1]][pos[0]]=[]
        
def remplir_grille(grille,nombre_thon,nombre_requin):
    """
    Modifie la grille en place en y ajoutant aléatoirement le nombre de thons et de requins entrés en paramètre puis la retourne
    :param grille:(list)
    :param nombre_thon:(int)
    :param nombre_requin:(int)
    :return: (list) 
    :Effet de bord: modifie la grille
    >>> g=grille_vide(4, 3)
    >>> g
    [[[], [], [], []], [[], [], [], []], [[], [], [], []]]
    >>> remplir_grille(g,2,1)
    [[[2], [], [], []], [[], [], [2], []], [[5, 3], [], [], []]]
    """
    global re,th
    re=nombre_requin
    th=nombre_thon
    positions=pos_possibles(grille)
    for nb_requin in range(nombre_requin):
        indice_case=randint(len(positions))
        coordonnees=positions[indice_case]
        ajoute_un_requin(grille,coordonnees)
        positions.pop(indice_case)
    for nb_thon in range(nombre_thon):
        indice_case=randint(len(positions))
        coordonnees=positions[indice_case]
        ajoute_un_thon(grille,coordonnees)
        positions.pop(indice_case)        
    return grille

def voisins(grille,position):
    """
    renvoie la liste des positions voisines (les quatres)
    :param grille:(list)
    :param pos:(tuple) (i,j) avec i et j entiers
    :return: (list)
    >>>g=grille_vide(4, 3)
    >>> voisins(g,(2,1))
    [(1, 1), (3, 1), (2, 0), (2, 2)]
    >>> voisins(g,(3,1))
    [(2, 1), (0, 1), (3, 0), (3, 2)]
    >>> voisins(g,(1,0))
    [(0, 0), (2, 0), (1, 2), (1, 1)]
    """
    xpos,ypos=position
    return [((xpos-1)%largeur(grille),ypos),((xpos+1)%largeur(grille),ypos),
            (xpos,(ypos-1)%hauteur(grille)),(xpos,(ypos+1)%hauteur(grille))]

def est_libre(grille,position):
    """
    retourne True si la case est inoccupée et False sinon
    :param grille:(list)
    :param pos:(tuple) (i,j) avec i et j entiers
    :return: (bool)
    
    >>>g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
    >>> est_libre(g,(2,1))
    False
    >>> est_libre(g,(1,0))
    True
    """
    return len(grille[position[1]][position[0]])==0

def est_thon(grille,position):
    """
    Retourne True si la case est occupée par un thon et False sinon
    :param grille:(list)
    :param pos:(tuple) (i,j) avec i et j entiers
    :return: (bool)
    
    >>>g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
    >>> est_thon(g,(2,1))
    True
    >>> est_thon(g,(1,0))
    False
    """
    return len(grille[position[1]][position[0]])==1

def est_requin(grille,position):
    """
    Retourne True si la case est occupée par un requin et False sinon
    :param grille:(list)
    :param pos:(tuple) (i,j) avec i et j entiers
    :return: (bool)
    
    >>>g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
    >>> est_requin(g,(3,0))
    True
    >>> est_requin(g,(1,0))
    False
    """
    return len(grille[position[1]][position[0]])==2

def voisins_libres(grille,position):
    """
    Renvoie la liste des positions voisines inoccupées
    :param grille:(list)
    :param pos:(tuple) (i,j) avec i et j entiers
    :return: (list)
    >>>g=g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
    >>> voisins_libres(g,(2,1))
    [(1, 1), (2, 0)]
    >>> voisins_libres(g,(3,1))
    []
    >>> voisins_libres(g,(1,0))
    [(2, 0), (1, 2), (1, 1)]
    """
    retour=[]
    xpos,ypos=position
    vois=voisins(grille,position)
    for voi in vois:
        if est_libre(grille,voi):
            retour.append(voi)
    return retour
   
def voisins_thons(grille,position):
    """renvoie la liste des positions voisines occupées par des thons
    :param grille:(list)
    :param pos:(tuple) (i,j) avec i et j entiers
    :return: (list)
    >>>g=g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
    >>>voisins_thons(g,(2,1))
    [(3, 1)]
    >>>voisins_thons(g,(3,1))
    [(2, 1), (0, 1), (3, 2)]
    >>>voisins_thons(g,(1,0))
    [(0, 0)]
    """
    retour=[]
    vois=voisins(grille,position)
    for voi in vois:
        if est_thon(grille,voi):
            retour.append(voi)
    return retour

def baisse_energie(grille,pos):
    """
    modifie la grille en place en diminuant le taux et retourne le taux
    :param grille:(List)
    :param pos:(tuple) (i,j) avec i et j entiers
    :return:(int) le taux
    >>> g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
    >>> baisse_energie(g,(3,0))
    2
    >>> g
    [[[2], [], [], [5, 2]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
    """
    grille[pos[1]][pos[0]][1]-=1
    return grille[pos[1]][pos[0]][1]

def baisse_gestation(grille,pos):
    """
    modifie la grille en place en diminuant le taux et retourne le taux
    :param grille:(List)
    :param pos:(tuple) (i,j) avec i et j entiers
    :return:(int) le taux
    >>>g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
    >>>baisse_gestation(g,(3,2))
    1
    >>>g
    [[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [1]]]
    """
    grille[pos[1]][pos[0]][0]-=1
    return grille[pos[1]][pos[0]][0]

def deplacer(grille,posd,posf):
    """
    Modifie la grille en place en déplaçant une case de la grille de la première position à la seconde et efface la première case
    :param grille:(list)
    :param posd:(tuple) (i,j) avec i et j entiers position de départ
    :param posf:(tuple) (i,j) avec i et j entiers position de fin
    :Effet de bord: modifie la liste
    >>>g=[[[2], [], [], [5, 3]], [[2], [], [2], [2]], [[5, 3], [], [5, 3], [2]]]
    >>>deplacer(g,(3,0),(3,1))
    >>>g
    >>>[[[2], [], [], []], [[2], [], [2], [5,3]], [[5, 3], [], [5, 3], [2]]]
    >>> deplacer(g,(3,2),(3,0))
    >>> g
    [[[2], [], [], [2]], [[2], [], [2], [5, 3]], [[5, 3], [], [5, 3], []]]
    """
    adeplacer=grille[posd[1]][posd[0]]
    grille[posf[1]][posf[0]]=adeplacer
    efface_une_case(grille,posd)
    
def position_aleatoire(liste):
    """
    Retourne une position aléatoire parmis une liste de positions fournie en paramètre
    :param liste:(List) liste de tuples
    :return:(tuple) une position
    >>> liste=[(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2), (2, 0), (2, 1), (2, 2), (3, 0), (3, 1), (3, 2)]
    >>> position_aleatoire(liste)
    (1, 2)
    """
    indice=randint(len(liste))
    p=liste[indice]
    return p

def comportement_thon(grille,position):
    """
    Modifie la grille en place pour prendre en compte le comportement du thon
    :param grille:(List)
    :param position:(tuple)
    :Effet de bord: modifie la grille
    """
    global th
    deplace=False
    nouvelle_position=position
    positions_possibles=voisins_libres(grille,position)
    if len(positions_possibles)!=0:
        nouvelle_position=position_aleatoire(positions_possibles)       
        deplacer(grille,position,nouvelle_position)       
        deplace=True
    gestation=baisse_gestation(grille,nouvelle_position)
    if gestation==0:
        if deplace:
            th+=1
            ajoute_un_thon(grille,position)
        ajoute_un_thon(grille,nouvelle_position)
        
def comportement_requin(grille,position):
    """
    Modifie la grille en place pour prendre en compte le comportement du requin
    :param grille:(List)
    :param position:(tuple)
    :Effet de bord: modifie la grille
    """
    global re,th
    deplace=False
    nouvelle_position=position
    energie=baisse_energie(grille,position)
    positions_thons=voisins_thons(grille,position)
    positions_possibles=voisins_libres(grille,position)
    if len(positions_thons)!=0:
        th-=1
        nouvelle_position=position_aleatoire(positions_thons)
        deplacer(grille,position,nouvelle_position)
        grille[nouvelle_position[1]][nouvelle_position[0]][1]=ENERGIE_REQUIN
        deplace=True
    elif len(positions_possibles)!=0:
        nouvelle_position=position_aleatoire(positions_possibles)
        deplacer(grille,position,nouvelle_position)
        deplace=True
    if energie==0:
        efface_une_case(grille,nouvelle_position)
        re-=1
    else:
        gestation=baisse_gestation(grille,nouvelle_position)
        if gestation==0:  
            if deplace:
                re+=1
                ajoute_un_requin(grille,position)
                grille[nouvelle_position[1]][nouvelle_position[0]][0]=TEMPS_DE_GESTATION_REQUIN

def pas_suivant(grille):
    """
    modifie la grille en place en choisissant une case puis en lui appliquant le comportement adapté
    :param grille:(List)
    :Effet de bord: modifie la grille
    """
    global pasencours
    pasencours+=1
    position=position_aleatoire((pos_possibles(grille)))
    if est_thon(grille,position):
        comportement_thon(grille,position)
            
    if est_requin(grille,position):
        comportement_requin(grille,position)
                    

def previsualisation(grille,i,l,h,p):
    """
    Préaffiche la grille en utilisant des pylab(sans show)
    :param grille:(list)
    :param i:(int) le pas
    :param l:(int) la largeur
    :param h:(int) la hauteur
    :param p:(int) le nombre de pas
    :CU:None
    """
    abst=[]
    ordt=[]
    absr=[]
    ordr=[]
    title('simulation pour largeur:'+str(l)+' hauteur:'+str(h)+' pas:'+str(p) +'\n'+'iteration:'+str(i))
    axis([-1,len(grille[0]),-1,len(grille)])
    for h in range(len(grille)):
        for l in range(len(grille[0])):
            if len(grille[h][l])==1:
                abst.append(l)
                ordt.append(len(grille)-h-1)
    plot(abst, ordt,"bo");
    for h in range(len(grille)):
        for l in range(len(grille[0])):
            if len(grille[h][l])==2:
                absr.append(l)
                ordr.append(len(grille)-h-1)
    plot(absr, ordr,"ro");
    draw()

def visualisation_mer(largeur,hauteur,pas):
    """
    Crée une grille de la bonne taille avec le bon pourcentage de poissons
    puis la fait évoluer le bon nombre de fois en appelant la prévisualisation  tout les 100 pas
    et pour finir affiche la visualisation
    :param largeur:(int)
    :param hauteur:(int)
    :param pas:(int)
    """
    taille=largeur*hauteur
    grille=remplir_grille(grille_vide(largeur,hauteur),(taille*POURC_THON)//100,(taille*POURC_REQUIN)//100)
    previsualisation(grille,0,largeur,hauteur,pas)
    clf()
    for i in range(pas*nombre_de_cases(grille)):
        pas_suivant(grille)
        if i%100==0:
            previsualisation(grille,i,largeur,hauteur,pas)
            pause(0.1)
            clf()
    show();
    
def simulation(largeur,hauteur,pas):
    """
    Crée une grille de la bonne taille avec le bon pourcentage de poissons puis affiche le graphique des effectifs de thon et de requins au cours de la simulation
    :param largeur:(int)
    :param hauteur:(int)
    :param pas:(int)
    """
    taille=largeur*hauteur
    grille=remplir_grille(grille_vide(largeur,hauteur),(taille*POURC_THON)//100,(taille*POURC_REQUIN)//100)
#    axis([-1,len(grille[0]),-1,len(grille)])
    ths=[th]
    res=[re]
    lesx=[pasencours]
    for i in range(pas*nombre_de_cases(grille)):
        pas_suivant(grille)
        ths.append(th)
        res.append(re)
        lesx.append(pasencours)
    plot(lesx,ths,"b-");
    plot(lesx,res,"r-");
    axis([-1,len(lesx),-1,max(max(ths),max(res))])
    title('simulation pour largeur:'+str(largeur)+' hauteur:'+str(hauteur)+' pas:'+str(pas))
    show();

```


